#pragma once
#ifndef _MYPAINT_CLASS_
#define _MYPAINT_CLASS_

#include "resource.h"
#include "stdafx.h"
#include <fstream>
#include <cmath>

#define MAX_ARR 100

using namespace std;

class _Object {
public:
	int x1, y1, x2, y2;
	COLORREF Color;
public:
	virtual void Draw(HDC hdc) = 0;
	void move(int x, int y) {
		x1 += x;
		x2 += x;
		y1 += y;
		y2 += y;
	}
	void MoveX1Y1(int x, int y) {
		x1 += x;
		y1 += y;
	}
	void MoveX1Y2(int x, int y) {
		x1 += x;
		y2 += y;
	}
	void MoveX2Y1(int x, int y) {
		x2 += x;
		y1 += y;
	}
	void MoveX2Y2(int x, int y) {
		x2 += x;
		y2 += y;
	}
	virtual void WriteToFile(ofstream &file) = 0;
	void DrawBorder(HDC hdc) {
		HPEN hPen = CreatePen(PS_DOT, 1, RGB(0, 0, 0));
		SelectObject(hdc, hPen);
		Rectangle(hdc, x1, y1, x2, y2);
		DeleteObject(hPen);
	}
	void DrawGreenBorder(HDC hdc) {
		HPEN hPen = CreatePen(PS_DOT, 1, RGB(0, 255, 0));
		SelectObject(hdc, hPen);
		Rectangle(hdc, x1, y1, x2, y2);
		DeleteObject(hPen);
		hPen = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
		SelectObject(hdc, hPen);
		int x3 = x1, y3 = y2, x4 = x2, y4 = y1;
		HBRUSH hBrush = CreateSolidBrush(RGB(0, 255, 0));
		HGDIOBJ hOldBrush = SelectObject(hdc, hBrush);
		Rectangle(hdc, x1 - 5, y1 - 5, x1 + 5, y1 + 5);
		Rectangle(hdc, x2 - 5, y2 - 5, x2 + 5, y2 + 5);
		Rectangle(hdc, x3 - 5, y3 - 5, x3 + 5, y3 + 5);
		Rectangle(hdc, x4 - 5, y4 - 5, x4 + 5, y4 + 5);
		SelectObject(hdc, hOldBrush);
		DeleteObject(hPen);
		DeleteObject(hBrush);
	}
	bool Check(int x, int y) {
		if (min(x1, x2) > x | max(x1, x2) < x) return 0;
		if (min(y1, y2) > y | max(y1, y2) < y) return 0;
		return 1;
	}
	bool CheckX1Y1(int x, int y) {
		if ((x1 - 5) > x | (x1 + 5) < x) return 0;
		if ((y1 - 5) > y | (y1 + 5) < y) return 0;
		return 1;
	}
	bool CheckX1Y2(int x, int y) {
		if ((x1 - 5) > x | (x1 + 5) < x) return 0;
		if ((y2 - 5) > y | (y2 + 5) < y) return 0;
		return 1;
	}
	bool CheckX2Y1(int x, int y) {
		if ((x2 - 5) > x | (x2 + 5) < x) return 0;
		if ((y1 - 5) > y | (y1 + 5) < y) return 0;
		return 1;
	}
	bool CheckX2Y2(int x, int y) {
		if ((x2 - 5) > x | (x2 + 5) < x) return 0;
		if ((y2 - 5) > y | (y2 + 5) < y) return 0;
		return 1;
	}
	virtual bool IsLine() = 0;
	virtual bool IsRectangle() = 0;
	virtual bool IsEllipse() = 0;
	virtual bool IsText() = 0;
};

class _Line :public _Object {
public:
	_Line(int x1, int y1, int x2, int y2, COLORREF color);
	void Draw(HDC hdc);
	void WriteToFile(ofstream &file) {
		int type = ID_DRAW_LINE;
		file.write((char*)&type, sizeof(type));
		file.write((char*)&x1, sizeof(x1));
		file.write((char*)&y1, sizeof(y1));
		file.write( (char*)&x2, sizeof(x2));
		file.write((char*)&y2, sizeof(y2));
		file.write((char*)&Color, sizeof(Color));
	}
	bool IsLine();
	bool IsRectangle();
	bool IsEllipse();
	bool IsText();
};

class _Rectangle :public _Object {
public:
	_Rectangle(int x1, int y1, int x2, int y2, COLORREF color);
	void Draw(HDC hdc);
	void WriteToFile(ofstream &file) {
		int type = ID_DRAW_RECTANGLE;
		file.write((char*)&type, sizeof(type));
		file.write((char*)&x1, sizeof(x1));
		file.write((char*)&y1, sizeof(y1));
		file.write((char*)&x2, sizeof(x2));
		file.write((char*)&y2, sizeof(y2));
		file.write((char*)&Color, sizeof(Color));
	}
	bool IsLine();
	bool IsRectangle();
	bool IsEllipse();
	bool IsText();
};

class _Ellipse :public _Object {
public:
	_Ellipse(int x1, int y1, int x2, int y2, COLORREF color);
	void Draw(HDC hdc);
	void WriteToFile(ofstream &file) {
		int type = ID_DRAW_ELLIPSE;
		file.write((char*)&type, sizeof(type));
		file.write((char*)&x1, sizeof(x1));
		file.write((char*)&y1, sizeof(y1));
		file.write((char*)&x2, sizeof(x2));
		file.write((char*)&y2, sizeof(y2));
		file.write((char*)&Color, sizeof(Color));
	}
	bool IsLine();
	bool IsRectangle();
	bool IsEllipse();
	bool IsText();
};

class _Text :public _Object {
public:
	LOGFONT Font;
	TCHAR Text[MAX_STRING];
public:
	_Text(int x1, int y1, int x2, int y2, COLORREF color, LOGFONT font, TCHAR* text);
	void Draw(HDC hdc);
	void WriteToFile(ofstream &file) {
		int type = ID_DRAW_TEXT;
		file.write((char*)&type, sizeof(type));
		file.write((char*)&x1, sizeof(x1));
		file.write((char*)&y1, sizeof(y1));
		file.write((char*)&x2, sizeof(x2));
		file.write((char*)&y2, sizeof(y2));
		file.write((char*)&Color, sizeof(Color));
		file.write((char*)&Font, sizeof(Font));
		file.write((char*)&Text, sizeof(Text));
	}
	bool IsLine();
	bool IsRectangle();
	bool IsEllipse();
	bool IsText();
};

class _MyPaint {
public:
	COLORREF Color;
	LOGFONT Font;
	int n;
	_Object** Objects;
public:
	_MyPaint();
	~_MyPaint();
	void AddObject(_Object* object);
	void DeleteObject(int pos);
	int CheckObject(int x, int y) {
		for (int i = n-1; i >= 0; --i)
			if (Objects[i]->Check(x, y)) return i;
		return -1;
	}
	void Load(ifstream &file);
	void Save(ofstream &file);
};

struct _LINE {
	int x1, x2, y1, y2;
	COLORREF Color;
};

struct _RECTANGLE {
	int x1, x2, y1, y2;
	COLORREF Color;
};

struct _ELLIPSE {
	int x1, x2, y1, y2;
	COLORREF Color;
};

struct _TEXT {
	int x1, x2, y1, y2;
	COLORREF Color;
	LOGFONT Font;
	TCHAR Text[MAX_STRING];
};

#endif // !_MYPAINT_CLASS_