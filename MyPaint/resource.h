//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MyPaint.rc
//
#define IDC_MYICON                      2
#define IDD_MYPAINT_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDS_MDIFRAME                    104
#define IDS_MDIDRAW                     105
#define IDI_MYPAINT                     107
#define IDC_MYPAINT                     109
#define IDM_ABOUT                       110
#define IDR_MAINFRAME                   128
#define IDB_TOOLBAR                     133
#define ID_TOOLBAR                      200
#define ID_FILE_NEW                     32771
#define ID_FILE_OPEN                    32772
#define ID_FILE_SAVE                    32773
#define ID_FILE_EXIT                    32774
#define ID_DRAW_COLOR                   32775
#define ID_DRAW_FONT                    32776
#define ID_DRAW_LINE                    32777
#define ID_DRAW_RECTANGLE               32778
#define ID_DRAW_ELLIPSE                 32779
#define ID_DRAW_TEXT                    32780
#define ID_DRAW_SELECTOBJECT            32781
#define ID_WINDOW_TILE                  32782
#define ID_WINDOW_CASCADE               32783
#define ID_WINDOW_CLOSEALL              32784
#define ID_EDIT_CUT                     32785
#define ID_EDIT_COPY                    32786
#define ID_EDIT_PASTE                   32787
#define ID_EDIT_DELETE                  32788
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32788
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
