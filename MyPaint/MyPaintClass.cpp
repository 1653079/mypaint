#include "stdafx.h"
#include "MyPaintClass.h"

_Line::_Line(int x1, int y1, int x2, int y2, COLORREF color) {
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	this->Color = color;
}

void _Line::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 1, Color);
	SelectObject(hdc, hPen);
	MoveToEx(hdc, x1, y1, NULL);
	LineTo(hdc, x2, y2);
	DeleteObject(hPen);
}

bool _Line::IsLine() {
	return 1;
}

bool _Line::IsRectangle() {
	return 0;
}

bool _Line::IsEllipse() {
	return 0;
}

bool _Line::IsText() {
	return 0;
}

_Rectangle::_Rectangle(int x1, int y1, int x2, int y2, COLORREF color) {
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	this->Color = color;
}

void _Rectangle::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 1, Color);
	SelectObject(hdc, hPen);
	Rectangle(hdc, x1, y1, x2, y2);
	DeleteObject(hPen);
}

bool _Rectangle::IsLine() {
	return 0;
}

bool _Rectangle::IsRectangle() {
	return 1;
}

bool _Rectangle::IsEllipse() {
	return 0;
}

bool _Rectangle::IsText() {
	return 0;
}

_Ellipse::_Ellipse(int x1, int y1, int x2, int y2, COLORREF color) {
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	this->Color = color;
}

void _Ellipse::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 1, Color);
	SelectObject(hdc, hPen);
	Ellipse(hdc, x1, y1, x2, y2);
	DeleteObject(hPen);
}

bool _Ellipse::IsLine() {
	return 0;
}

bool _Ellipse::IsRectangle() {
	return 0;
}

bool _Ellipse::IsEllipse() {
	return 1;
}

bool _Ellipse::IsText() {
	return 0;
}

_Text::_Text(int x1, int y1, int x2, int y2, COLORREF color, LOGFONT font, TCHAR* text) {
	this->x1 = x1;
	this->y1 = y1;
	this->x2 = x2;
	this->y2 = y2;
	this->Color = color;
	this->Font = font;
	_tcscpy_s(this->Text, text);
}

void _Text::Draw(HDC hdc) {
	HFONT hFont = CreateFontIndirect(&Font);
	SelectObject(hdc, hFont);
	SetTextColor(hdc, Color);
	RECT rc;
	rc.left = x1;
	rc.right = x2;
	rc.top = y1;
	rc.bottom = y2;
	bool flag = 0;
	for (int i = 0; i < MAX_STRING; ++i) {
		if (Text[i] == '\n') flag = 1;
	}
	if (flag) DrawText(hdc, Text, _tcslen(Text), &rc, DT_CENTER);
	else DrawText(hdc, Text, _tcslen(Text), &rc, DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	DeleteObject(hFont);
}

bool _Text::IsLine() {
	return 0;
}

bool _Text::IsRectangle() {
	return 0;
}

bool _Text::IsEllipse() {
	return 0;
}

bool _Text::IsText() {
	return 1;
}

_MyPaint::_MyPaint() {
	Color = RGB(0, 0, 0);
	ZeroMemory(&Font, sizeof(Font));
	n = 0;
	Objects = new _Object*[MAX_ARR];
}

_MyPaint::~_MyPaint() {
	delete[] Objects;
}

void _MyPaint::AddObject(_Object* object) {
	if (n <= MAX_ARR) {
		Objects[n] = object;
		++n;
	}
	else {
		_Object** tmp = Objects;
		Objects = new _Object*[++n];
		for (int i = 0; i < n - 1; ++i) {
			Objects[i] = tmp[i];
		}
		Objects[n - 1] = object;
		delete[] tmp;
	}
}

void _MyPaint::DeleteObject(int pos) {
	for (int i = pos; i < n; ++i) {
		Objects[i] = Objects[i + 1];
	}
	--n;
}

void _MyPaint::Load(ifstream &file) {
	file.read((char*)&Color, sizeof(Color));
	file.read((char*)&Font, sizeof(Font));
	int k;
	file.read((char*)&k, sizeof(k));
	for (int i = 0; i < k; ++i) {
		int type;
		file.read((char*)&type, sizeof(type));
		int x1, y1, x2, y2;
		COLORREF color;
		LOGFONT font;
		TCHAR text[MAX_STRING];
		switch (type)
		{
		case ID_DRAW_LINE:
			{
				file.read((char*)&x1, sizeof(x1));
				file.read((char*)&y1, sizeof(y1));
				file.read((char*)&x2, sizeof(x2));
				file.read((char*)&y2, sizeof(y2));
				file.read((char*)&color, sizeof(color));
				_Line* tmp = new _Line(x1, y1, x2, y2, color);
				AddObject(tmp);
			}
			break;
		case ID_DRAW_RECTANGLE:
			{
				file.read((char*)&x1, sizeof(x1));
				file.read((char*)&y1, sizeof(y1));
				file.read((char*)&x2, sizeof(x2));
				file.read((char*)&y2, sizeof(y2));
				file.read((char*)&color, sizeof(color));
				_Rectangle* tmp = new _Rectangle(x1, y1, x2, y2, color);
				AddObject(tmp);
			}
			break;
		case ID_DRAW_ELLIPSE:
			{
				file.read((char*)&x1, sizeof(x1));
				file.read((char*)&y1, sizeof(y1));
				file.read((char*)&x2, sizeof(x2));
				file.read((char*)&y2, sizeof(y2));
				file.read((char*)&color, sizeof(color));
				_Ellipse* tmp = new _Ellipse(x1, y1, x2, y2, color);
				AddObject(tmp);
			}
			break;
		case ID_DRAW_TEXT:
			{
				file.read((char*)&x1, sizeof(x1));
				file.read((char*)&y1, sizeof(y1));
				file.read((char*)&x2, sizeof(x2));
				file.read((char*)&y2, sizeof(y2));
				file.read((char*)&color, sizeof(color));
				file.read((char*)&font, sizeof(font));
				file.read((char*)&text, sizeof(text));
				_Text* tmp = new _Text(x1, y1, x2, y2, color, font, text);
				AddObject(tmp);
			}
			break;
		}
	}
}

void _MyPaint::Save(ofstream &file) {
	file.write((char*)&Color, sizeof(Color));
	file.write((char*)&Font, sizeof(Font));
	file.write((char*)&n, sizeof(n));
	for (int i = 0; i < n; ++i) {
		Objects[i]->WriteToFile(file);
	}
}