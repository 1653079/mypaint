#include "stdafx.h"
#include "MyPaint.h"
#include "MyPaintClass.h"

#define MAX_LOADSTRING 100

HINSTANCE hInst;
HWND hMDIFrame;
HWND hMDIClient;
HWND hClient = NULL;
HFONT hFont;

WCHAR szTitle[MAX_LOADSTRING];
WCHAR szMDIFrameClass[MAX_LOADSTRING];
WCHAR szMDIDrawClass[MAX_LOADSTRING];

UINT nChild = 1;
int DrawMode = ID_DRAW_LINE;

ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    MDIFrameProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK    MDIDrawProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	MDICloseProc(HWND, LPARAM);

void OnCreate(HWND, HWND&);
void OnNotify(LPARAM lParam);
void OnSize(LPARAM, HWND);
void OnNew();
void OnOpen(HWND);
void OnSave(HWND);
void OnColor(HWND);
void OnFont(HWND);
void OnSelectDrawMode(HWND, HWND, int);
void OnCut();
void OnCopy();
void OnPaste();
void OnDelete();

int APIENTRY wWinMain(_In_ HINSTANCE	 hInstance,
                      _In_opt_ HINSTANCE hPrevInstance,
                      _In_ LPWSTR		 lpCmdLine,
                      _In_ int			 nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDS_MDIFRAME, szMDIFrameClass, MAX_LOADSTRING);
	LoadStringW(hInstance, IDS_MDIDRAW, szMDIDrawClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYPAINT));

    MSG msg;

    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateMDISysAccel(hMDIClient, &msg) && !TranslateAccelerator(hMDIFrame, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = MDIFrameProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MYPAINT);
    wcex.lpszClassName  = szMDIFrameClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_MYPAINT));

    if (!RegisterClassExW(&wcex)) return FALSE;

	wcex.lpfnWndProc = MDIDrawProc;
	wcex.cbWndExtra = 8;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = szMDIDrawClass;
	wcex.hIconSm = NULL;

	if (!RegisterClassExW(&wcex)) return FALSE;
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance;

   hMDIFrame = CreateWindowW(szMDIFrameClass, szTitle, WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, nullptr, nullptr, hInstance, nullptr);
   
   if (!hMDIFrame)
   {
      return FALSE;
   }

   ShowWindow(hMDIFrame, nCmdShow);
   UpdateWindow(hMDIFrame);

   return TRUE;
}

LRESULT CALLBACK MDIFrameProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static HWND hToolbar;
    switch (message)
    {
	case WM_CREATE:
		OnCreate(hWnd, hToolbar);
		break;
	case WM_NOTIFY:
		OnNotify(lParam);
		break;
	case WM_SIZE:
		OnSize(lParam, hToolbar);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            switch (wmId)
            {
			case ID_FILE_NEW:
				OnNew();
				break;
			case ID_FILE_OPEN:
				OnOpen(hWnd);
				break;
			case ID_FILE_SAVE:
				OnSave(hWnd);
				break;
            case ID_FILE_EXIT:
                DestroyWindow(hWnd);
                break;
			case ID_EDIT_CUT:
				OnCut();
				break;
			case ID_EDIT_COPY:
				OnCopy();
				break;
			case ID_EDIT_PASTE:
				OnPaste();
				break;
			case ID_EDIT_DELETE:
				OnDelete();
				break;
			case ID_DRAW_COLOR:
				OnColor(hWnd);
				break;
			case ID_DRAW_FONT:
				OnFont(hWnd);
				break;
			case ID_DRAW_LINE:
				OnSelectDrawMode(hWnd, hToolbar, ID_DRAW_LINE);
				break;
			case ID_DRAW_RECTANGLE:
				OnSelectDrawMode(hWnd, hToolbar, ID_DRAW_RECTANGLE);
				break;
			case ID_DRAW_ELLIPSE:
				OnSelectDrawMode(hWnd, hToolbar, ID_DRAW_ELLIPSE);
				break;
			case ID_DRAW_TEXT:
				OnSelectDrawMode(hWnd, hToolbar, ID_DRAW_TEXT);
				break;
			case ID_DRAW_SELECTOBJECT:
				OnSelectDrawMode(hWnd, hToolbar, ID_DRAW_SELECTOBJECT);
				break;
			case ID_WINDOW_TILE:
				SendMessage(hMDIClient, WM_MDITILE, MDITILE_SKIPDISABLED, 0L);
				break;
			case ID_WINDOW_CASCADE:
				SendMessage(hMDIClient, WM_MDICASCADE, MDITILE_SKIPDISABLED, 0L);
				break;
			case ID_WINDOW_CLOSEALL:
				EnumChildWindows(hMDIClient, (WNDENUMPROC)MDICloseProc, 0L);
				break;
            default:
				return DefFrameProc(hWnd, hMDIClient, message, wParam, lParam);
            }
        }
		return DefFrameProc(hWnd, hMDIClient, message, wParam, lParam);
    default:
		return DefFrameProc(hWnd, hMDIClient, message, wParam, lParam);
    }
    return 0;
}

LRESULT CALLBACK MDIDrawProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static int x1, y1, x2, y2;
	static HWND hText = NULL;
	static int obj = -1;
	static int mode = -1;
	static int zx, zy;
	if (DrawMode != ID_DRAW_SELECTOBJECT && mode != -1) {
		obj = -1;
		mode = -1;
		InvalidateRgn(hClient, NULL, TRUE);
	}
	switch (message)
	{
	case ID_EDIT_CUT:
		{
		if (obj == -1 | mode != 0) break;
		_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hWnd, 0);
		if (OpenClipboard(hWnd)) {
			if (data->Objects[obj]->IsLine()) {
				UINT i = RegisterClipboardFormat(L"MyPaint::_LINE");
				EmptyClipboard();
				HANDLE hData = GlobalAlloc(GHND, sizeof(_LINE));
				_LINE* tmp = (_LINE*)GlobalLock(hData);
				tmp->x1 = data->Objects[obj]->x1;
				tmp->y1 = data->Objects[obj]->y1;
				tmp->x2 = data->Objects[obj]->x2;
				tmp->y2 = data->Objects[obj]->y2;
				tmp->Color = data->Objects[obj]->Color;
				GlobalUnlock(hData);
				SetClipboardData(i, hData);
			}
			else if (data->Objects[obj]->IsRectangle()) {
				UINT i = RegisterClipboardFormat(L"MyPaint::_RECTANGLE");
				EmptyClipboard();
				HANDLE hData = GlobalAlloc(GHND, sizeof(_RECTANGLE));
				_RECTANGLE* tmp = (_RECTANGLE*)GlobalLock(hData);
				tmp->x1 = data->Objects[obj]->x1;
				tmp->y1 = data->Objects[obj]->y1;
				tmp->x2 = data->Objects[obj]->x2;
				tmp->y2 = data->Objects[obj]->y2;
				tmp->Color = data->Objects[obj]->Color;
				GlobalUnlock(hData);
				SetClipboardData(i, hData);
			}
			else if (data->Objects[obj]->IsEllipse()) {
				UINT i = RegisterClipboardFormat(L"MyPaint::_ELLIPSE");
				EmptyClipboard();
				HANDLE hData = GlobalAlloc(GHND, sizeof(_ELLIPSE));
				_ELLIPSE* tmp = (_ELLIPSE*)GlobalLock(hData);
				tmp->x1 = data->Objects[obj]->x1;
				tmp->y1 = data->Objects[obj]->y1;
				tmp->x2 = data->Objects[obj]->x2;
				tmp->y2 = data->Objects[obj]->y2;
				tmp->Color = data->Objects[obj]->Color;
				GlobalUnlock(hData);
				SetClipboardData(i, hData);
			}
			else if (data->Objects[obj]->IsText()) {
				UINT i = RegisterClipboardFormat(L"MyPaint::_TEXT");
				EmptyClipboard();
				HANDLE hData = GlobalAlloc(GHND, sizeof(_TEXT));
				_TEXT* tmp = (_TEXT*)GlobalLock(hData);
				tmp->x1 = data->Objects[obj]->x1;
				tmp->y1 = data->Objects[obj]->y1;
				tmp->x2 = data->Objects[obj]->x2;
				tmp->y2 = data->Objects[obj]->y2;
				tmp->Color = data->Objects[obj]->Color;
				tmp->Font = ((_Text*)data->Objects[obj])->Font;
				_tcscpy_s(tmp->Text, ((_Text*)data->Objects[obj])->Text);
				GlobalUnlock(hData);
				SetClipboardData(i, hData);
			}
		}
		CloseClipboard();
		data->DeleteObject(obj);
		obj = -1;
		mode = -1;
		HMENU hMenu = GetMenu(hMDIFrame);
		EnableMenuItem(hMenu, ID_EDIT_CUT, MF_DISABLED | MF_BYCOMMAND);
		EnableMenuItem(hMenu, ID_EDIT_COPY, MF_DISABLED | MF_BYCOMMAND);
		EnableMenuItem(hMenu, ID_EDIT_DELETE, MF_DISABLED | MF_BYCOMMAND);
		InvalidateRect(hWnd, NULL, TRUE);
		}
		break;
	case ID_EDIT_COPY:
		{
			if (obj == -1 | mode != 0) break;
			_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hWnd, 0);
			if (OpenClipboard(hWnd)) {
				if (data->Objects[obj]->IsLine()) {
					UINT i = RegisterClipboardFormat(L"MyPaint::_LINE");
					EmptyClipboard();
					HANDLE hData = GlobalAlloc(GHND, sizeof(_LINE));
					_LINE* tmp = (_LINE*)GlobalLock(hData);
					tmp->x1 = data->Objects[obj]->x1;
					tmp->y1 = data->Objects[obj]->y1;
					tmp->x2 = data->Objects[obj]->x2;
					tmp->y2 = data->Objects[obj]->y2;
					tmp->Color = data->Objects[obj]->Color;
					GlobalUnlock(hData);
					SetClipboardData(i, hData);
				}
				else if (data->Objects[obj]->IsRectangle()) {
					UINT i = RegisterClipboardFormat(L"MyPaint::_RECTANGLE");
					EmptyClipboard();
					HANDLE hData = GlobalAlloc(GHND, sizeof(_RECTANGLE));
					_RECTANGLE* tmp = (_RECTANGLE*)GlobalLock(hData);
					tmp->x1 = data->Objects[obj]->x1;
					tmp->y1 = data->Objects[obj]->y1;
					tmp->x2 = data->Objects[obj]->x2;
					tmp->y2 = data->Objects[obj]->y2;
					tmp->Color = data->Objects[obj]->Color;
					GlobalUnlock(hData);
					SetClipboardData(i, hData);
				}
				else if (data->Objects[obj]->IsEllipse()) {
					UINT i = RegisterClipboardFormat(L"MyPaint::_ELLIPSE");
					EmptyClipboard();
					HANDLE hData = GlobalAlloc(GHND, sizeof(_ELLIPSE));
					_ELLIPSE* tmp = (_ELLIPSE*)GlobalLock(hData);
					tmp->x1 = data->Objects[obj]->x1;
					tmp->y1 = data->Objects[obj]->y1;
					tmp->x2 = data->Objects[obj]->x2;
					tmp->y2 = data->Objects[obj]->y2;
					tmp->Color = data->Objects[obj]->Color;
					GlobalUnlock(hData);
					SetClipboardData(i, hData);
				}
				else if (data->Objects[obj]->IsText()) {
					UINT i = RegisterClipboardFormat(L"MyPaint::_TEXT");
					EmptyClipboard();
					HANDLE hData = GlobalAlloc(GHND, sizeof(_TEXT));
					_TEXT* tmp = (_TEXT*)GlobalLock(hData);
					tmp->x1 = data->Objects[obj]->x1;
					tmp->y1 = data->Objects[obj]->y1;
					tmp->x2 = data->Objects[obj]->x2;
					tmp->y2 = data->Objects[obj]->y2;
					tmp->Color = data->Objects[obj]->Color;
					tmp->Font = ((_Text*)data->Objects[obj])->Font;
					_tcscpy_s(tmp->Text, ((_Text*)data->Objects[obj])->Text);
					GlobalUnlock(hData);
					SetClipboardData(i, hData);
				}
			}
			CloseClipboard();
		}
		break;
	case ID_EDIT_PASTE:
		{
			if (OpenClipboard(hWnd)) {
				_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hWnd, 0);

				UINT i1 = RegisterClipboardFormat(L"MyPaint::_LINE");
				HANDLE hData1 = (HANDLE)GetClipboardData(i1);
				if (hData1 != NULL)
				{
					_LINE* dt = (_LINE*)GlobalLock(hData1);
					_Line* tmp = new _Line(dt->x1, dt->y1, dt->x2, dt->y2, dt->Color);
					data->AddObject(tmp);
					GlobalUnlock(hData1);
				}

				UINT i2 = RegisterClipboardFormat(L"MyPaint::_RECTANGLE");
				HANDLE hData2 = (HANDLE)GetClipboardData(i2);
				if (hData2 != NULL)
				{
					_RECTANGLE* dt = (_RECTANGLE*)GlobalLock(hData2);
					_Rectangle* tmp = new _Rectangle(dt->x1, dt->y1, dt->x2, dt->y2, dt->Color);
					data->AddObject(tmp);
					GlobalUnlock(hData2);
				}

				UINT i3 = RegisterClipboardFormat(L"MyPaint::_ELLIPSE");
				HANDLE hData3 = (HANDLE)GetClipboardData(i3);
				if (hData3 != NULL)
				{
					_ELLIPSE* dt = (_ELLIPSE*)GlobalLock(hData3);
					_Ellipse* tmp = new _Ellipse(dt->x1, dt->y1, dt->x2, dt->y2, dt->Color);
					data->AddObject(tmp);
					GlobalUnlock(hData3);
				}

				UINT i4 = RegisterClipboardFormat(L"MyPaint::_TEXT");
				HANDLE hData4 = (HANDLE)GetClipboardData(i4);
				if (hData4 != NULL)
				{
					_TEXT* dt = (_TEXT*)GlobalLock(hData4);
					_Text* tmp = new _Text(dt->x1, dt->y1, dt->x2, dt->y2, dt->Color, dt->Font, dt->Text);
					data->AddObject(tmp);
					GlobalUnlock(hData4);
				}
				
				if (hData1 == NULL && hData2 == NULL && hData3 == NULL && hData4 == NULL) {
					HANDLE hData5 = (HANDLE)GetClipboardData(CF_TEXT);
					if (hData5 != NULL) {
						char* text1 = (char*)GlobalLock(hData5);
						TCHAR text2[MAX_STRING];
						swprintf_s(text2, L"%hs", text1);
						_Text* tmp = new _Text(0, 0, 500, 100, data->Color, data->Font, text2);
						data->AddObject(tmp);
						GlobalUnlock(hData5);
					}
				}
				InvalidateRgn(hClient, NULL, TRUE);
			}
			CloseClipboard();
		}
		break;
	case ID_EDIT_DELETE:
		{
			if (obj == -1 | mode != 0) break;
			_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hWnd, 0);
			data->DeleteObject(obj);
			obj = -1;
			mode = -1;
			HMENU hMenu = GetMenu(hMDIFrame);
			EnableMenuItem(hMenu, ID_EDIT_CUT, MF_DISABLED | MF_BYCOMMAND);
			EnableMenuItem(hMenu, ID_EDIT_COPY, MF_DISABLED | MF_BYCOMMAND);
			EnableMenuItem(hMenu, ID_EDIT_DELETE, MF_DISABLED | MF_BYCOMMAND);
			InvalidateRect(hWnd, NULL, TRUE);
		}
		break;
	case WM_MDIACTIVATE:
		{
			hClient = hWnd;
			obj = -1;
			mode = -1;
			HMENU hMenu = GetMenu(hMDIFrame);
			EnableMenuItem(hMenu, ID_EDIT_PASTE, MF_ENABLED | MF_BYCOMMAND);
		}
		break;
	case WM_DESTROY:
		{
			HMENU hMenu = GetMenu(hMDIFrame);
			EnableMenuItem(hMenu, ID_EDIT_PASTE, MF_DISABLED | MF_BYCOMMAND);
			return DefMDIChildProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_CREATE:
		{
		++nChild;
		_MyPaint* data = new _MyPaint;
		SetWindowLongPtr(hWnd, 0, (LONG_PTR)data);
		}
		break;
	case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc = BeginPaint(hWnd, &ps);
			_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hWnd, 0);
			for (int i = 0; i < data->n; ++i) {
				data->Objects[i]->Draw(hdc);
			}
			SetROP2(hdc, R2_COPYPEN);
			HBRUSH hBrush = (HBRUSH)GetStockObject(HOLLOW_BRUSH);
			HGDIOBJ hOldBrush = SelectObject(hdc, hBrush);
			if (obj != -1 && mode == 0) data->Objects[obj]->DrawGreenBorder(hdc);
			else if (obj != -1 && mode == -1) data->Objects[obj]->DrawBorder(hdc);
			SelectObject(hdc, hOldBrush);
			DeleteObject(hBrush);
			EndPaint(hWnd, &ps);
		}
		break;
	case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);
			switch (wmId)
			{
			default:
				return DefMDIChildProc(hWnd, message, wParam, lParam);
			}
		}
		break;
	case WM_LBUTTONDOWN:
		{
		if (hClient != NULL && hWnd != hClient) return 0;
			if (hText != NULL) {
				TCHAR text[MAX_LOADSTRING];
				int length = GetWindowTextW(hText, text, MAX_LOADSTRING);
				ShowWindow(hText, SW_HIDE);
				DestroyWindow(hText);
				DeleteObject(hFont);
				HDC hdc = GetDC(hWnd);
				_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hWnd, 0);
				HFONT hFont = CreateFontIndirect(&data->Font);
				SelectObject(hdc, hFont);
				SetTextColor(hdc, data->Color);
				RECT rc;
				rc.left = x1;
				rc.right = x2;
				rc.top = y1;
				rc.bottom = y2;
				DrawText(hdc, text, length, &rc, DT_CENTER);
				DeleteObject(hFont);
				ReleaseDC(hWnd, hdc);
				hText = NULL;
				_Text* tmp = new _Text(x1, y1, x2, y2, data->Color, data->Font, text);
				data->AddObject(tmp);
				InvalidateRgn(hClient, NULL, TRUE);
			}
			x1 = x2 = LOWORD(lParam);
			y1 = y2 = HIWORD(lParam);
			if (DrawMode == ID_DRAW_SELECTOBJECT && mode == 0) {
				_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hWnd, 0);
				if (data->n == 0) break;
				zx = x1;
				zy = y1;
				if (data->Objects[obj]->CheckX1Y1(x1, y1)) mode = 1;
				else if (data->Objects[obj]->CheckX1Y2(x1, y1)) mode = 2;
				else if (data->Objects[obj]->CheckX2Y1(x1, y1)) mode = 3;
				else if (data->Objects[obj]->CheckX2Y2(x1, y1)) mode = 4;
				else if (data->Objects[obj]->Check(x1, y1)) mode = 5;
			}
		}
		break;
	case WM_MOUSEMOVE:
		{
		if (hClient != NULL && hWnd != hClient) return 0;
			HDC hdc = GetDC(hWnd);
			SetROP2(hdc, R2_NOTXORPEN);
			_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hWnd, 0);
			switch (DrawMode) {
			case ID_DRAW_LINE:
				{
					if (!(wParam&MK_LBUTTON)) break;
					HPEN hPen = CreatePen(PS_SOLID, 1, data->Color);
					SelectObject(hdc, hPen);
					MoveToEx(hdc, x1, y1, NULL);
					LineTo(hdc, x2, y2);
					x2 = LOWORD(lParam);
					y2 = HIWORD(lParam);
					MoveToEx(hdc, x1, y1, NULL);
					LineTo(hdc, x2, y2);
					DeleteObject(hPen);
				}
				break;
			case ID_DRAW_RECTANGLE:
				{
					if (!(wParam&MK_LBUTTON)) break;
					HPEN hPen = CreatePen(PS_SOLID, 1, data->Color);
					SelectObject(hdc, hPen);
					Rectangle(hdc, x1, y1, x2, y2);
					x2 = LOWORD(lParam);
					y2 = HIWORD(lParam);
					Rectangle(hdc, x1, y1, x2, y2);
					DeleteObject(hPen);
				}
				break;
			case ID_DRAW_ELLIPSE:
				{
					if (!(wParam&MK_LBUTTON)) break;
					HPEN hPen = CreatePen(PS_SOLID, 1, data->Color);
					SelectObject(hdc, hPen);
					Ellipse(hdc, x1, y1, x2, y2);
					x2 = LOWORD(lParam);
					y2 = HIWORD(lParam);
					Ellipse(hdc, x1, y1, x2, y2);
					DeleteObject(hPen);
				}
				break;
			case ID_DRAW_TEXT:
				{
					if (!(wParam&MK_LBUTTON)) break;
					HPEN hPen = CreatePen(PS_DOT, 1, RGB(0, 0, 0));
					SelectObject(hdc, hPen);
					Rectangle(hdc, x1, y1, x2, y2);
					x2 = LOWORD(lParam);
					y2 = HIWORD(lParam);
					Rectangle(hdc, x1, y1, x2, y2);
					DeleteObject(hPen);
				}
				break;
			case ID_DRAW_SELECTOBJECT:
				{
					int x = LOWORD(lParam);
					int y = HIWORD(lParam);
					_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hWnd, 0);
					if (data->n == 0) break;
					if (mode == -1) {
						int tmp = data->CheckObject(x, y);
						if (obj != tmp) {
							obj = tmp;
							InvalidateRgn(hClient, NULL, TRUE);
						}
					}
					else if (mode == 1) {
						if (!(wParam&MK_LBUTTON)) break;
						data->Objects[obj]->MoveX1Y1(x - zx, y - zy);
						zx = x;
						zy = y;
						InvalidateRgn(hClient, NULL, TRUE);
					}
					else if (mode == 2) {
						if (!(wParam&MK_LBUTTON)) break;
						data->Objects[obj]->MoveX1Y2(x - zx, y - zy);
						zx = x;
						zy = y;
						InvalidateRgn(hClient, NULL, TRUE);
					}
					else if (mode == 3) {
						if (!(wParam&MK_LBUTTON)) break;
						data->Objects[obj]->MoveX2Y1(x - zx, y - zy);
						zx = x;
						zy = y;
						InvalidateRgn(hClient, NULL, TRUE);
					}
					else if (mode == 4) {
						if (!(wParam&MK_LBUTTON)) break;
						data->Objects[obj]->MoveX2Y2(x - zx, y - zy);
						zx = x;
						zy = y;
						InvalidateRgn(hClient, NULL, TRUE);
					}
					else if (mode == 5) {
						if (!(wParam&MK_LBUTTON)) break;
						data->Objects[obj]->move(x - zx, y - zy);
						zx = x;
						zy = y;
						InvalidateRgn(hClient, NULL, TRUE);
					}
				}
				break;
			}
			ReleaseDC(hWnd, hdc);
		}
		break;
	case WM_LBUTTONUP:
		{
		if (hClient != NULL && hWnd != hClient) return 0;
			_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hWnd, 0);
			if (data == NULL) break;
			switch (DrawMode) {
			case ID_DRAW_LINE:
			{
				_Line* tmp = new _Line(x1, y1, x2, y2, data->Color);
				data->AddObject(tmp);
				InvalidateRgn(hClient, NULL, TRUE);
			}
			break;
			case ID_DRAW_RECTANGLE:
			{
				_Rectangle* tmp = new _Rectangle(x1, y1, x2, y2, data->Color);
				data->AddObject(tmp);
				InvalidateRgn(hClient, NULL, TRUE);
			}
			break;
			case ID_DRAW_ELLIPSE:
			{
				_Ellipse* tmp = new _Ellipse(x1, y1, x2, y2, data->Color);
				data->AddObject(tmp);
				InvalidateRgn(hClient, NULL, TRUE);
			}
			break;
			case ID_DRAW_TEXT:
				if (DrawMode == ID_DRAW_TEXT && hText == NULL) {
					hText = CreateWindow(L"EDIT", NULL, WS_CHILD | WS_BORDER, (x1 <= x2) ? x1 : x2, (y1 <= y2) ? y1 : y2, abs(x1 - x2), abs(y1 - y2), hWnd, NULL, hInst, NULL);
					hFont = CreateFontIndirect(&(data->Font));
					SendMessage(hText, WM_SETFONT, (WPARAM)hFont, TRUE);
					ShowWindow(hText, SW_SHOW);
				}
				break;
			case ID_DRAW_SELECTOBJECT:
				{
						_MyPaint * data = (_MyPaint*)GetWindowLongPtr(hWnd, 0);
						int x = LOWORD(lParam);
						int y = HIWORD(lParam);
						obj = data->CheckObject(x, y);
						if (obj == -1) {
							mode = -1;
							obj = -1;
							HMENU hMenu = GetMenu(hMDIFrame);
							EnableMenuItem(hMenu, ID_EDIT_CUT, MF_DISABLED | MF_BYCOMMAND);
							EnableMenuItem(hMenu, ID_EDIT_COPY, MF_DISABLED | MF_BYCOMMAND);
							EnableMenuItem(hMenu, ID_EDIT_DELETE, MF_DISABLED | MF_BYCOMMAND);
						}
						else {
							mode = 0;
							HMENU hMenu = GetMenu(hMDIFrame);
							EnableMenuItem(hMenu, ID_EDIT_CUT, MF_ENABLED | MF_BYCOMMAND);
							EnableMenuItem(hMenu, ID_EDIT_COPY, MF_ENABLED | MF_BYCOMMAND);
							EnableMenuItem(hMenu, ID_EDIT_DELETE, MF_ENABLED | MF_BYCOMMAND);
						}
						InvalidateRgn(hClient, NULL, TRUE);
				}
				break;
			}
		}
		break;
	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK MDICloseProc(HWND hMDIChild, LPARAM lParam)
{
	SendMessage(hMDIClient, WM_MDIDESTROY, (WPARAM)hMDIChild, 0L);
	return 1;
}

void OnCreate(HWND hWnd, HWND &hToolbar) {
	CLIENTCREATESTRUCT ccs;
	ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 3);
	ccs.idFirstChild = 50000;
	hMDIClient = CreateWindow(L"MDICLIENT", NULL, WS_CHILD | WS_BORDER, 0, 0, 0, 0, hWnd, (HMENU)0xCAC, hInst, (LPSTR)&ccs);
	ShowWindow(hMDIClient, SW_SHOW);
	
	hToolbar = CreateWindowEx(0, TOOLBARCLASSNAME, NULL, WS_CHILD | WS_BORDER | TBSTYLE_FLAT | TBSTYLE_TOOLTIPS, 0, 0, 0, 0, hWnd, NULL, hInst, NULL);
	HIMAGELIST g_hImageList = ImageList_LoadBitmap(hInst, MAKEINTRESOURCE(IDB_TOOLBAR), 24, 0, RGB(255, 0, 255));
	SendMessage(hToolbar, TB_SETIMAGELIST, 0, (LPARAM)g_hImageList);
	TBBUTTON tbButtons[] =
	{
		{ 0, ID_FILE_NEW,  TBSTATE_ENABLED, TBSTYLE_FLAT, 0, 0 },
		{ 1, ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_FLAT, 0, 0 },
		{ 2, ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_FLAT, 0, 0 },
		{ -1, 0, TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 3, ID_DRAW_LINE, TBSTATE_ENABLED | TBSTATE_CHECKED, TBSTYLE_FLAT, 0, 0 },
		{ 4, ID_DRAW_RECTANGLE , TBSTATE_ENABLED, TBSTYLE_FLAT, 0, 0 },
		{ 5, ID_DRAW_ELLIPSE, TBSTATE_ENABLED, TBSTYLE_FLAT, 0, 0 },
		{ 6, ID_DRAW_TEXT, TBSTATE_ENABLED, TBSTYLE_FLAT, 0, 0 },
		{ 7, ID_DRAW_SELECTOBJECT, TBSTATE_ENABLED, TBSTYLE_FLAT, 0, 0 }
	};
	SendMessage(hToolbar, TB_BUTTONSTRUCTSIZE, (WPARAM)sizeof(TBBUTTON), 0);
	SendMessage(hToolbar, TB_ADDBUTTONS, sizeof(tbButtons)/sizeof(TBBUTTON), (LPARAM)&tbButtons);
}

void OnNotify(LPARAM lParam) {
	LPTOOLTIPTEXT   lpToolTipText;
	TCHAR			szToolTipText[MAX_LOADSTRING];

	lpToolTipText = (LPTOOLTIPTEXT)lParam;
	if (lpToolTipText->hdr.code == TTN_NEEDTEXT) {
		LoadString(hInst, lpToolTipText->hdr.idFrom, szToolTipText, MAX_LOADSTRING);
		lpToolTipText->lpszText = szToolTipText;
	}
}

void OnSize(LPARAM lParam, HWND hToolbar) {
	UINT w, h;
	w = LOWORD(lParam);
	h = HIWORD(lParam);
	SetWindowPos(hToolbar, NULL, 0, 0, w, 34, SWP_SHOWWINDOW);
	MoveWindow(hMDIClient, 0, 34, w, h + 34, TRUE);
}

void OnNew() {
	WCHAR title[MAX_LOADSTRING];
	wsprintf(title, L"Noname-%d.drw", nChild);
	MDICREATESTRUCT mcs;
	mcs.hOwner = hInst;
	mcs.szClass = szMDIDrawClass;
	mcs.szTitle = title;
	mcs.style = MDIS_ALLCHILDSTYLES;
	mcs.x = mcs.cx = CW_USEDEFAULT;
	mcs.y = mcs.cy = CW_USEDEFAULT;
	mcs.lParam = NULL;
	SendMessage(hMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mcs);
}

void OnOpen(HWND hWnd) {
	OPENFILENAME ofn;
	TCHAR szFile[256];
	TCHAR szFilter[] = TEXT("MyPaint file\0*.drw\0");
	szFile[0] = '\0';
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile= szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetOpenFileName(&ofn)) {
		MDICREATESTRUCT mcs;
		mcs.hOwner = hInst;
		mcs.szClass = szMDIDrawClass;
		mcs.szTitle = szFile;
		mcs.style = MDIS_ALLCHILDSTYLES;
		mcs.x = mcs.cx = CW_USEDEFAULT;
		mcs.y = mcs.cy = CW_USEDEFAULT;
		mcs.lParam = NULL;
		SendMessage(hMDIClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mcs);
		_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hClient, 0);
		ifstream file;
		file.open(szFile, ios::binary);
		data->Load(file);
		file.close();
		InvalidateRgn(hClient, NULL, TRUE);
	}	
}

void OnSave(HWND hWnd) {
	OPENFILENAME ofn;
	TCHAR szFile[256];
	TCHAR szFilter[] = TEXT("MyPaint file\0*.drw\0");
	szFile[0] = '\0';
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile;
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrTitle = L"Save";
	ofn.Flags = OFN_OVERWRITEPROMPT;
	if (GetSaveFileName(&ofn)) {
		_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hClient, 0);
		ofstream file;
		WCHAR tmp[MAX_LOADSTRING];
		wsprintf(tmp, L"%s.drw", szFile);
		file.open(tmp, ios::binary);
		data->Save(file);
		file.close();
	}
}

void OnSelectDrawMode(HWND hWnd, HWND hToolbar, int mode) {
	HMENU hMenu = GetMenu(hWnd);
	CheckMenuItem(hMenu, DrawMode, MF_UNCHECKED | MF_BYCOMMAND);
	SendMessage(hToolbar, TB_CHECKBUTTON, DrawMode, FALSE);
	DrawMode = mode;
	CheckMenuItem(hMenu, DrawMode, MF_CHECKED | MF_BYCOMMAND);
	SendMessage(hToolbar, TB_CHECKBUTTON, DrawMode, TRUE);
}

void OnColor(HWND hWnd) {
	static COLORREF acrCustClr[16];
	CHOOSECOLOR cc;
	ZeroMemory(&cc, sizeof(CHOOSECOLOR));
	cc.lStructSize = sizeof(CHOOSECOLOR);
	cc.hwndOwner = hWnd;
	cc.lpCustColors = (LPDWORD)acrCustClr;
	cc.rgbResult = RGB(0, 0, 0);
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;
	if (ChooseColor(&cc)) {
		_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hClient, 0);
		if (data != NULL) data->Color = cc.rgbResult;
	}
}

void OnFont(HWND hWnd) {
	LOGFONT lf;
	ZeroMemory(&lf, sizeof(lf));
	CHOOSEFONT cf;
	ZeroMemory(&cf, sizeof(CHOOSEFONT));
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.hwndOwner = hWnd;
	cf.lpLogFont = &lf;
	cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
	if (ChooseFont(&cf)) {
		_MyPaint* data = (_MyPaint*)GetWindowLongPtr(hClient, 0);
		if (data!= NULL) data->Font = lf;
	}
}

void OnCut() {
	if (hClient != NULL) SendMessage(hClient, ID_EDIT_CUT, NULL, NULL);
}

void OnCopy() {
	if (hClient != NULL) SendMessage(hClient, ID_EDIT_COPY, NULL, NULL);
}

void OnPaste() {
	if (hClient != NULL) SendMessage(hClient, ID_EDIT_PASTE, NULL, NULL);
}

void OnDelete() {
	if (hClient != NULL) SendMessage(hClient, ID_EDIT_DELETE, NULL, NULL);
}